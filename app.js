const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');

const app = express();

// Slack values
const clientId = "14275508756.265631316769";
const clientSecret = "fba2af4085f50217b9f8be01cf3a1767";
const verifyToken = "TMEXW2rX6O8OcYXU2F2V0H57";
const oAuthAccessTOken = "xoxp-14275508756-36962372368-267315192519-92105db85ce4b771e2cfc32334e988b2";
const botAccessToken = "xoxb-266282164692-tbmb57gqT3KC5tfsmtpWUMZg";
const botUserId = "U7U8A4ULC";


// Interceptors ======================
// Verify origin
var verifier = function(req, resp, next) {
	let incomingToken = req.body.token;
	if (req.url.indexOf('/inputs') > -1) {
		// verify token only comes in on command requests
		next();
	} else if (incomingToken === verifyToken) {
		next();
	} else {
		console.log("Could not verify token '" + incomingToken + "'; rejecting");
		resp.sendStatus(404);
	}
};
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(verifier);


// Routes ============================
app.post('/challenger/challenge', function (req, resp) {
	var parseChallengeMessage = function (rawText) {
		var matchGroups = rawText.match(challengeRegex);
		if (matchGroups && matchGroups.length > 1) {
			var competitionText = '';
			var users = [];
			for (var i = 1; i < matchGroups.length; i++) {
				var eachPart = matchGroups[i];
				if (!eachPart) {
					continue;
				}
				if (eachPart.startsWith('<@')) {
					var userList = eachPart.split(/\s+/).map(getUserName);
					users = users.concat(userList);
				} else {
					competitionText = eachPart;
				}
			};

			return {
				parsed: true,
				competition: competitionText,
				challengees: users
			};

		} else {
			return {parsed: false};
		}
	};
	var getUserName = function(encodedUser) {
		var groups = encodedUser.trim().match(userRegex);
		if (groups && groups[1]) {
			return groups[1];
		} else {
			console.log("Could not parse '" + encodedUser + "' as a user; returning empty string");
			return '';
		}
	}

	const challengeRegex = /^([^\<]+)\s+(\<@[A-Z0-9]+\|[a-z\.\-_]+\>(?:\s\<@[A-Z0-9]+\|[a-z0-9\.\-_]+\>)*)?$/;
	const userRegex = /^<@[A-Z0-9]+\|([a-z0-9\.\-_]+)\>$/;

	var rawText = req.body.text ? req.body.text.trim() : '';

	if (!rawText) {
		const usageMessage = "Oops! You need to at least specify the challenge. You probably should specify the challengees "
		+ "as well, like this: /challenge foosball! @giuse @corley @tyler";
		resp.send(usageMessage);
	} else	if (rawText === "help") {
		const helpMessage = "Command format: /challenge competition @user [@user]"
		+ "\nOpen challenge to anyone in the channel to foosball: /challenge foosball"
		+ "\nChallenge specific people: /challenge foosball @tyler OR /challenge halo @tyler @devin @joseph";
		resp.send(helpMessage);
	} else {
		var parsedMessage = parseChallengeMessage(rawText);
		if (parsedMessage.parsed) {
			var challenger = req.body.user_name;

			var challengees = parsedMessage.challengees.reduce(function(part1, part2) {
				return part1 + ", " + part2;
			});
			var lastCommaIndex = challengees.lastIndexOf(',');
			if (lastCommaIndex !== -1) {
				var fullLength = challengees.length;
				challengees = challengees.slice(0, lastCommaIndex) + " and" + challengees.slice(lastCommaIndex + 1, fullLength);
			}

			var challengeMessage = challenger + " has challenged " + challengees  + " to " + parsedMessage.competition;
			resp.send({
				response_type: "in_channel",
				text: challengeMessage
			});
		} else {
			const badFormatMessage = "Oops! I can't tell what you're saying. Try this syntax: /challenge foosball! @giuse";
			resp.send(badFormatMessage);
		}
	}
});

const activeNoses = {};
app.post('/challenger/nosegoes', function (req, resp) {
	var params = req.body.text ? req.body.text.trim() : '';

	var loserCount = 1;
	if (params && params.match(/^\d+$/).length > 0) {
		loserCount = +params;
	} else if (params && !params.match(/^\d+$/).length > 0) {
		console.log("Couldn't parse loser count from '" + params + "'");
		resp.send("I can't tell how many losers you specified.");
		return;
	} else {
		loserCount = 1;
	}
	var message = req.body.user_name + " has started a \"nose goes\" contest!"
	+ (loserCount === 1 ? " Don't be the last one to touch your nose!" : " Don't be one of the last " + loserCount + " to touch your nose!");

	var noseId = req.body.user_id + "-" + (+new Date());
	var isChannel = req.body.channel_id.startsWith('C');
	var channelId = req.body.channel_id;
	var getUserIds = isChannel ? getUserIdsInChannel : getUserIdsInGroup;
	getUserIds(channelId, function(userIds) {
		if (userIds.indexOf(botUserId) > -1) {
			userIds.splice(userIds.indexOf(botUserId), 1);
		}	

		if (userIds.length === 1) {
			resp.send("You're the only one here. You win by default!");
		} else {
			var nose = {
				id: noseId,
				creator: req.body.user_id,
				loserCount: loserCount,
				remainingUsers: userIds,
				safeUsers: [],
				createdAt: +new Date(),
				createdIn: channelId,
				isChannel: isChannel
			};
			activeNoses[noseId] = nose;

			resp.send({
				text: message,
				response_type: "in_channel",
				attachments: [{
					callback_id: noseId,
					actions: [{
						name: "touchNose",
						text: "Nose!",
						type: "button",
						value: "notMe"
					}]
				}]
			});
		}
	});
});

function handleNotMe(btnPayload, resp) {
	function sendReceivedMessage() {
		resp.send({
			text: btnPayload.original_message.text,
			attachments: [{
				text: "You've got your hand on your nose! Hope you weren't the last one!"
			}]
		});
	}
	function nameTheLosers(nose) {
		delete activeNoses[nose.id];

		loadUserNames(nose.remainingUsers.concat([nose.creator]), function(nameMap) {
			var creatorName = nameMap[nose.creator];
			delete nameMap[nose.creator];
			var losers = "";
			for (var key in nameMap) {
				losers += nameMap[key] + ", ";
			}
			// chop off last comma
			losers = losers.substring(0, losers.length - 2);
			// Replace last comma with and
			var lastCommaIndex = losers.lastIndexOf(',');
			if (lastCommaIndex > -1) {
				losers = losers.substring(0, lastCommaIndex) + " and" + losers.substring(lastCommaIndex + 1, losers.length);
			}

			var message = losers + (nose.remainingUsers.length > 1 ? " were " : " was ") + "the last to touch their nose in "
			+ creatorName + "'s \"nose goes\" challenge. Tough break.";

			postMessage(nose.createdIn, message);
		});
	}

	var userId = btnPayload.user.id;
	var noseId = btnPayload.callback_id;
	if (noseId && noseId in activeNoses) {
		var nose = activeNoses[noseId];
		if  (nose.remainingUsers.length === nose.loserCount) {
			sendReceivedMessage();
			nameTheLosers(nose);
		} else {
			var userIndex = nose.remainingUsers.indexOf(userId);
			var removed = nose.remainingUsers.splice(userIndex, 1);
			nose.safeUsers = nose.safeUsers.concat(removed);

			sendReceivedMessage();

			if  (nose.remainingUsers.length === nose.loserCount) {
				nameTheLosers(nose);
			}
		}
	} else {
		console.log("Could not find nose ID of " + noseId + "; sending back confusion to user");
		resp.send("Hmmmm, I see that you touched your nose, but I don't know which competition this is for. Sorry!");
	}
};

app.post('/challenger/inputs', function(req, resp){
	// TODO: Make this detect which button to route to the correct handler
	var payload = JSON.parse(req.body.payload);
	handleNotMe(payload, resp);
});


// API calls ===========================================================
const slackBaseApi = "https://volleymetrics.slack.com/api";
function getUserIdsInChannel(channelId, callback) {
	request.get(slackBaseApi + "/channels.info?channel=" + channelId, function(err, resp, body) {
		var bodyJson = JSON.parse(body);
		callback(bodyJson.channel.members);
	}).auth(null, null, true, botAccessToken);
}

function getUserIdsInGroup(groupId, callback) {
	request.get(slackBaseApi + "/groups.info?channel=" + groupId, function(err, resp, body) {
		var bodyJson = JSON.parse(body);
		callback(bodyJson.group.members);
	}).auth(null, null, true, botAccessToken);
}

function loadUserNames(userIds, callback) {
	var usersMap = {};
	var callsWaiting = userIds.length;
	userIds.forEach(function(userId) {
		request.get(slackBaseApi + "/users.info?user=" + userId, function (err, resp, body){
			var bodyJson = JSON.parse(body);
			usersMap[userId] = bodyJson.user.real_name;
			callsWaiting--;
			if (callsWaiting === 0) {
				callback(usersMap);
			}
		
		}).auth(null, null, true, botAccessToken)
	});
}

function postMessage(channelId, message) {
	var body = {
		channel: channelId,
		text: message
	};
	request.post(slackBaseApi + "/chat.postMessage").form(body).auth(null, null, true, botAccessToken)
}

// Run ===========================================================
app.listen(3000, function() {
	console.log("Challenger Slack app running.");
});
