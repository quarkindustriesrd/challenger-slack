# The Challenger: A Slack App #

Adds some custom commands to Slack:

* /challenge: Allows users to challenge other users to some competition
* /nosegoes: All users are given a notification and a button to push. Last one to push the button is "it."

(This started as a professional development project)